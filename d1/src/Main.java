public class Main {
    public static void main(String[] args) {
        //Each instance of a class in java is/should independent to one another.

        Car car1 = new Car();

        /*car1.make = "Veyron";
        car1.brand = "Bugatti";
        car1.price = 2000000;

        System.out.println(car1.brand);
        System.out.println(car1.make);
        System.out.println(car1.price);*/

        Car car2 = new Car();

        /*car2.make = "Tamaraw FX";
        car2.brand = "Toyota";
        car2.price = 450000;

        System.out.println(car2.brand);
        System.out.println(car2.make);
        System.out.println(car2.price);*/

        Car car3 = new Car();

       /* car3.make = "Beetle";
        car3.brand = "Volkswagen";
        car3.price = 3000000;

        System.out.println(car3.brand);
        System.out.println(car3.make);
        System.out.println(car3.price);*/

        Car car4 = new Car();
        /*System.out.println(car4.brand);*/

        //Initialized using parameterized constructor.
        //Car car5 = new Car("Vios","Toyota",1500000);

        Car car5 = new Car();
        /*System.out.println(car5.brand);
        System.out.println(car5.make);
        System.out.println(car5.price);*/

        //Driver driver1 =  new Car("Alejandro",30);

        Driver driver = new Driver("Alejandro",21);
        car5.setCarDriver(driver);

        car1.start();
        car2.start();
        car3.start();
        car4.start();
        //car5.start();
        System.out.println(car1.getMake());
        //System.out.println(car5.getMake());

        //car5.setMake("Innova");
        //System.out.println(car5.getMake());

        car1.setMake("Veyron");
        car1.setBrand("Bugatti");
        car1.setPrice(2000000);
        System.out.println(car1.getMake());
        System.out.println(car1.getBrand());
        System.out.println(car1.getPrice());

        Driver newDriver = new Driver("Antonio",21);
        car1.setCarDriver(newDriver);
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        //custom getCarDriverName method.
        System.out.println(car5.getCarDriverName());

        /* ************************************************************ */
        /*                        AM ACTIVITY                           */
        Animal newAnimal = new Animal("Lion","GoldenYellow");
        Animal animal1 = newAnimal;

        System.out.println(animal1.getName());
        System.out.println(animal1.getColor());

        System.out.println(animal1.call());
        /* *********************************************************** */

    }
}