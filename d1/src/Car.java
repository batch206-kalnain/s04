public class Car {
    /* ***********************************************************************************
    * Classes should have properties/attributes.
    * Properties/Attributes - the characteristics of the object the class will create.
    *
    * Constructor - method to create the object and insatiate with its initialized values.
    *
    * Getters and Setters - are method to get the value of an objects.
    * Method - actions that an object can perform or do.
    * ***********************************************************************************/
    private String make;
    private String brand;
    private int price;

    private Driver carDriver;

    /* *******************************************************************************************
    * Constructor - is a method which allows us to set the inital values of an instance.
    * Empty/default constructor - allows us to create an instance with default initialized values.
    *
    * By default, Java, when your class does not have a constructor, assigns one for you. Java
    * also sets the default values. You could have a way to add your own default values.
    * *******************************************************************************************/

    /* *********************************************************************************************
    * public access modifier - the variable/property in the class is accessible anywhere in the
    * application.
    *
    * attributes/properties of a class should not be made public. They should only be accessed with
    * getters and setters instead of just dot notation.
    *
    * private - limits the access and ability to get or set a variable/method to only within its own
    * class.
    *
    * getters - methods that return the value of the property
    * setters - methods that allows us to set the value of a property
    * *********************************************************************************************/
    public Car(){
        //this.brand = "Geely";
        this.carDriver = new Driver();
    }

    public Car(String make, String brand, int price, Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }
    /* **********************************************************************************
    * Getter and Setter for our properties.
    * Getters return a value so therefore we must add the dataType of the value returned.
    * **********************************************************************************/
    public String getMake(){
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.make;
    }

    /*
    * You could set a property as read-only, this means that the value of the propert can only
    * be get but not updated. By removing or commenting out the setter.
    */

    public String getBrand(){
        return this.brand;
    }

    public int getPrice(){
        return this.price;
    }

    public void setMake(String makeParams){
        this.make = makeParams;
    }
    public void setBrand(String brandParams){
        this.brand = brandParams;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }

    /* ***********************************************************************************
    * Method are function of an object/instance which allows us to perform certain task.
    * void - means that the function does not return anything. Because in Java, a method's
    * return dataType must also be declared.
    * ***********************************************************************************/
    public void start(){
        System.out.println(this.brand+" Autobots transform!");
    }
	
	/* ***********************************************************************************
    * Classes have relationship
	*
	* Composition allows modelling object to be made up of other objects. Classes can have
	* instances of other classes.
    * ***********************************************************************************/

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    //Custom method to retrieve the car driver's name:
    public String getCarDriverName(){
        return this.carDriver.getName();
    }
}
